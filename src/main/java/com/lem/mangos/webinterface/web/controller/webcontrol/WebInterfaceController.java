package com.lem.mangos.webinterface.web.controller.webcontrol;

import com.lem.mangos.webinterface.mangos.dto.UserDetailsDto;
import com.lem.mangos.webinterface.web.controller.constant.ModelAttributes;
import com.lem.mangos.webinterface.web.controller.constant.Templates;
import com.lem.mangos.webinterface.web.controller.constant.URLMapping;
import com.lem.mangos.webinterface.web.service.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = URLMapping.WEB_CONTROL)
public class WebInterfaceController {
	
	@Autowired
	UserDetailsService userDetailsService;
	
	
	@GetMapping(URLMapping.ROOT)
	public String webControlRoot(){
		return Templates.WEB_CONTROL;
	}
	
	@GetMapping(URLMapping.CREATE_USER)
	public String createUser( Model model ){
		model.addAttribute(ModelAttributes.CREATED_USER, false);
		return Templates.CREATE_USER;
	}
	
	@RequestMapping(value = URLMapping.CREATE_USER, method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String createUser(UserDetailsDto userDetailsDto, Model model){ //@RequestBody
		userDetailsService.saveDTO(userDetailsDto);
		model.addAttribute(ModelAttributes.CREATED_USER, true);
		return Templates.CREATE_USER;
	}
	
}
