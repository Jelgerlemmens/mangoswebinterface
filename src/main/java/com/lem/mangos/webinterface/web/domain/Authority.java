package com.lem.mangos.webinterface.web.domain;

public enum Authority {
	ADMIN("admin");
	
	
	public final String authority;
	
	private Authority(String authority){
		this.authority = authority;
	}
	
	public String getAuthority() {
		return authority;
	}
	
	public static Authority getAuthority(String authority){
		for(Authority a : values()){
			if(a.authority.equals(authority)){
				return a;
			}
		}
		return null;
	}
}
