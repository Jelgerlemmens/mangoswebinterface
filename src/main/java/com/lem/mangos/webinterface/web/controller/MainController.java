package com.lem.mangos.webinterface.web.controller;

import com.lem.mangos.webinterface.web.controller.constant.ModelAttributes;
import com.lem.mangos.webinterface.web.controller.constant.URLMapping;
import com.lem.mangos.webinterface.web.controller.constant.Templates;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {
	
	@GetMapping(URLMapping.LOGIN)
	public String login(){
		return Templates.LOGIN;
	}
	
	
	@RequestMapping(URLMapping.LOGIN_ERROR)
	public String loginError(Model model) {
		model.addAttribute(ModelAttributes.LOGIN_ERROR, true);
		return Templates.LOGIN;
	}
	
}
