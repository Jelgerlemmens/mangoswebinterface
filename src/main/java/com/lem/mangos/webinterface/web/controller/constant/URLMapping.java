package com.lem.mangos.webinterface.web.controller.constant;

public class URLMapping {
	
	//base
	public static final String ROOT = "/";
	public static final String INDEX = "/index";
	public static final String LOGIN = "/login";
	public static final String LOGOUT = "/logout";
	public static final String LOGIN_ERROR = "login-error";
	
	//class url mappings
	public static final String HOME = "/home";
	public static final String WEB_CONTROL = "/webcontrol";
	public static final String ACCOUNT_CONTROL = "/accountcontrol";
	public static final String CHARACTER_CONTROL = "/charcontrol";
	public static final String WORLD_CONTROL = "/worldcontrol";
	public static final String RAW_COMMAND_CONTROL = "/rawcommand";
	
	//web endpoint mapping
	public static final String CREATE_USER = "/createuser";
	public static final String AUTHORITIES = "/authorities";
	
	
}
