package com.lem.mangos.webinterface.web.service;

import com.lem.mangos.webinterface.web.domain.AuthorityImpl;
import com.lem.mangos.webinterface.web.repo.AuthorityRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorityService {
	
	@Autowired
	private AuthorityRepo authorityRepo;
	
	public AuthorityImpl save(AuthorityImpl authorityImpl){
		return authorityRepo.save(authorityImpl);
	}
	
}
