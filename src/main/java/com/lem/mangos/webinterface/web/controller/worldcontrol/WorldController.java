package com.lem.mangos.webinterface.web.controller.worldcontrol;

import com.lem.mangos.webinterface.web.controller.constant.Templates;
import com.lem.mangos.webinterface.web.controller.constant.URLMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = URLMapping.WORLD_CONTROL)
public class WorldController {
	
	
	@GetMapping(URLMapping.ROOT)
	public String worldControlRoot(){
		return Templates.WORLD_CONTROL;
	}
	
}
