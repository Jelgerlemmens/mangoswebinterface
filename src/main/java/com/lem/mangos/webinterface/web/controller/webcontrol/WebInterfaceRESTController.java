package com.lem.mangos.webinterface.web.controller.webcontrol;


import java.util.Arrays;
import java.util.List;

import com.lem.mangos.webinterface.web.controller.constant.URLMapping;
import com.lem.mangos.webinterface.web.domain.Authority;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WebInterfaceRESTController {
	
	
	@GetMapping(URLMapping.AUTHORITIES)
	public List<Authority> getAuthorities(){
		return Arrays.asList(Authority.values());
	}
	
}
