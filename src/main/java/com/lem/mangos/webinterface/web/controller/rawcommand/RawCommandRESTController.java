package com.lem.mangos.webinterface.web.controller.rawcommand;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Objects;

import javax.xml.soap.SOAPException;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.lem.mangos.webinterface.mangos.domain.Commands;
import com.lem.mangos.webinterface.mangos.domain.XmlSoapEnvelope;
import com.lem.mangos.webinterface.mangos.dto.CommandDto;
import com.lem.mangos.webinterface.mangos.service.CommandService;
import com.lem.mangos.webinterface.mangos.service.SoapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RawCommandRESTController {
	
	@Autowired
	SoapService soapService;
	
	@Autowired
	CommandService commandService;
	
	@Autowired
	URI mangosHost;
	
	private XmlMapper mapper = XmlMapper.builder().build();
	
	@GetMapping(value = "send_command")
	public String sendCommand(@RequestParam(value = "command") String command) throws IOException, SOAPException {
		soapService.createConnection(mangosHost.toString());
		XmlSoapEnvelope xmlSoapEnvelope = soapService.sendMessage(command);
		soapService.disconnect();
		return mapper.writeValueAsString(xmlSoapEnvelope);
	}
	
	@GetMapping(value = "get_commands")
	public List<CommandDto> getCommandList(){
		return commandService.getCommands();
	}
	
	@GetMapping(value = "get_command")
	public CommandDto getCommand(@RequestParam(value = "command_string", required = false) String command,
								 @RequestParam(value = "command_id", required = false) Integer commandId ){
		if(Objects.isNull(command) && Objects.isNull(commandId)){
			return null;
		}else if(Objects.isNull(command) && Objects.nonNull(commandId)){
			return Commands.getAsDto(commandId);
		}else if(Objects.isNull(commandId) && !command.isEmpty()){
			return Commands.getAsDto(command);
		}
		return null;
	}
	
	@GetMapping(value = "get_syntax")
	public String getSyntax(@RequestParam(value = "command_string", required = false) String command,
							@RequestParam(value = "command_id", required = false) Integer commandId ){
		if(command.isEmpty() && Objects.isNull(commandId)){
			return null;
		}else if(command.isEmpty() && Objects.nonNull(commandId)){
			return Commands.getSyntaxForId(commandId);
		}else if(!command.isEmpty() && Objects.isNull(commandId)){
			return Commands.valueOf(command).getSyntax();
		}
		return null;
	}
	
}
