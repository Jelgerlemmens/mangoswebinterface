package com.lem.mangos.webinterface.web.controller.accountcontrol;

import com.lem.mangos.webinterface.web.controller.constant.Templates;
import com.lem.mangos.webinterface.web.controller.constant.URLMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = URLMapping.ACCOUNT_CONTROL)
public class AccountController {
	
	@GetMapping(URLMapping.ROOT)
	public String accountRoot(){
		return Templates.ACCOUNT_CONTROL;
	}
	
}
