package com.lem.mangos.webinterface.web.controller.rawcommand;

import com.lem.mangos.webinterface.web.controller.constant.Templates;
import com.lem.mangos.webinterface.web.controller.constant.URLMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = URLMapping.RAW_COMMAND_CONTROL)
public class RawCommandController {
	
	
	@GetMapping(URLMapping.ROOT)
	public String rawCommandControlRoot(){
		return Templates.RAW_COMMAND_CONTROL;
	}
	
}
