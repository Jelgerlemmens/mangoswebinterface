package com.lem.mangos.webinterface.web.controller.constant;

public class Templates {
	
	public static final String LOGIN = "login.html";
	public static final String HOME = "home.html";
	public static final String ACCOUNT_CONTROL = "account_control.html";
	public static final String WEB_CONTROL = "web_control.html";
	public static final String RAW_COMMAND_CONTROL = "raw_command.html";
	public static final String WORLD_CONTROL = "world_control.html";
	public static final String CHARACTER_CONTROL = "character_control.html";
	
	
	public static final String CREATE_USER = "createuser.html";
	
}
