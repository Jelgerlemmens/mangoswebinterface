package com.lem.mangos.webinterface.web.repo;

import com.lem.mangos.webinterface.web.domain.UserDetailsImpl;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDetailsRepo extends CrudRepository<UserDetailsImpl, Long> {
}
