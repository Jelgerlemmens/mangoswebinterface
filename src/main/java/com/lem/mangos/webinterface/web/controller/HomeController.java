package com.lem.mangos.webinterface.web.controller;

import com.lem.mangos.webinterface.web.controller.constant.Templates;
import com.lem.mangos.webinterface.web.controller.constant.URLMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = URLMapping.HOME)
public class HomeController {
	
	
	@GetMapping(URLMapping.ROOT)
	public String home(){
		return Templates.HOME;
	}
	
	
}
