package com.lem.mangos.webinterface.web.service;

import java.util.Iterator;

import com.lem.mangos.webinterface.mangos.dto.UserDetailsDto;
import com.lem.mangos.webinterface.web.domain.Authority;
import com.lem.mangos.webinterface.web.domain.UserDetailsImpl;
import com.lem.mangos.webinterface.web.repo.UserDetailsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {
	
	@Autowired
	UserDetailsRepo userDetailsRepo;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return this.findByUsername(username);
	}
	
	public UserDetailsImpl findByUsername(String username) {
		Iterable<UserDetailsImpl> all = userDetailsRepo.findAll();
		Iterator<UserDetailsImpl> it = all.iterator();
		while(it.hasNext()){
			UserDetailsImpl u = it.next();
			if(u.getUsername().equals(username)){
				return u;
			}
		}
		return new UserDetailsImpl();
	}
	
	public UserDetailsImpl saveDTO(UserDetailsDto userDetailsDto){
		UserDetailsImpl impl = new UserDetailsImpl();
		impl.setUsername(userDetailsDto.getUsername());
		impl.setPassword(bCryptPasswordEncoder.encode(userDetailsDto.getPassword()));
		impl.setEnabled(userDetailsDto.isEnabled());
		impl.addAuthority(Authority.getAuthority(userDetailsDto.getAuthority()));
		impl.setCredentialsNonExpired(userDetailsDto.isCredentialsNonExpired());
		impl.setAccountNonLocked(userDetailsDto.isAccountNonLocked());
		impl.setAccountNonExpired(userDetailsDto.isAccountNonExpired());
		impl.setEmail(userDetailsDto.getEmail());
		return this.save(impl);
	}
	
	public UserDetailsImpl save(UserDetailsImpl user){
		return userDetailsRepo.save(user);
	}
	
	
}
