package com.lem.mangos.webinterface.web.controller.constant;

public class ModelAttributes {
	
	public static final String LOGIN_ERROR = "loginError";
	public static final String CREATED_USER = "createdUser";
}
