package com.lem.mangos.webinterface.web.controller.charcontrol;


import com.lem.mangos.webinterface.web.controller.constant.Templates;
import com.lem.mangos.webinterface.web.controller.constant.URLMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = URLMapping.CHARACTER_CONTROL)
public class CharacterController {
	
	
	@GetMapping(URLMapping.ROOT)
	public String characterControlRoot(){
		return Templates.CHARACTER_CONTROL;
	}

}
