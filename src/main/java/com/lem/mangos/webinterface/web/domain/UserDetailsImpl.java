package com.lem.mangos.webinterface.web.domain;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.lang.NonNull;

@Entity
@Table(name = "webUserDetails")
public class UserDetailsImpl implements org.springframework.security.core.userdetails.UserDetails {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "userDetailsId")
	private int userDetailsId;
	
	@Column(name = "username")
	@NonNull
	private String username;
	
	@Column(name = "password")
	@NonNull
	private String password;
	
	@OneToMany(cascade= CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "webUserAuthorities", joinColumns = @JoinColumn(name = "authorityId"), inverseJoinColumns = @JoinColumn(name = "userDetailsId"))
	private Collection<AuthorityImpl> authorities = new ArrayList<>();
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "accountNonExpired")
	private boolean accountNonExpired;
	
	@Column(name = "accountNonLocked")
	private boolean accountNonLocked;
	
	@Column(name = "credentialsNonExpired")
	private boolean credentialsNonExpired;
	
	@Column(name = "enabled")
	private boolean enabled;
	
	public UserDetailsImpl(){}
	
	public UserDetailsImpl(@NonNull String username, @NonNull String password, Collection<AuthorityImpl> authorities, String email, boolean accountNonExpired, boolean accountNonLocked, boolean credentialsNonExpired, boolean enabled) {
		this.username = username;
		this.password = password;
		this.authorities = authorities;
		this.email = email;
		this.accountNonExpired = accountNonExpired;
		this.accountNonLocked = accountNonLocked;
		this.credentialsNonExpired = credentialsNonExpired;
		this.enabled = enabled;
	}
	
	public UserDetailsImpl(@NonNull String username, @NonNull String password, String email) {
		this.username = username;
		this.password = password;
		this.email = email;
	}
	
	public int getUserDetailsId() {
		return userDetailsId;
	}
	
	public void setAuthorities(Collection<AuthorityImpl> authorities) {
		this.authorities = authorities;
	}
	
	public void addAuthority(Authority authority){
		this.authorities.add(new AuthorityImpl(authority.getAuthority()));
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}
	
	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}
	
	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	@Override
	public Collection<AuthorityImpl> getAuthorities() {
		return this.authorities;
	}
	
	@Override
	public String getPassword() {
		return this.password;
	}
	
	@Override
	public String getUsername() {
		return this.username;
	}
	
	@Override
	public boolean isAccountNonExpired() {
		return this.accountNonExpired;
	}
	
	@Override
	public boolean isAccountNonLocked() {
		return this.accountNonLocked;
	}
	
	@Override
	public boolean isCredentialsNonExpired() {
		return this.credentialsNonExpired;
	}
	
	@Override
	public boolean isEnabled() {
		return this.enabled;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
}
