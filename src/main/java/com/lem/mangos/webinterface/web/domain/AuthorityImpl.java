package com.lem.mangos.webinterface.web.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name = "webAuthorities")
public class AuthorityImpl implements GrantedAuthority {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "authorityId")
	private int authorityId;
	
	@Column(name = "authority")
	private String authority;
	
	public AuthorityImpl() {
	}
	
	public AuthorityImpl(String authority) {
		this.authority = authority;
	}
	
	public int getAuthorityId() {
		return authorityId;
	}
	
	public void setAuthorityId(int authorityId) {
		this.authorityId = authorityId;
	}
	
	@Override
	public String getAuthority() {
		return authority;
	}
	
	public void setAuthority(String authority) {
		this.authority = authority;
	}
	
}
