package com.lem.mangos.webinterface.web.repo;

import com.lem.mangos.webinterface.web.domain.AuthorityImpl;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorityRepo extends CrudRepository<AuthorityImpl, Long> {

}
