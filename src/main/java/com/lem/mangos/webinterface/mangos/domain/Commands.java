package com.lem.mangos.webinterface.mangos.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.lem.mangos.webinterface.mangos.dto.CommandDto;


public enum Commands {
	ACCOUNT("account","Syntax: .account Display the access level of your account.","0" ,1),
	ACCOUNT_CHARACTERS("account characters","Syntax: .account characters [#accountId|$accountName] Show list all characters for account selected by provided #accountId or $accountName, or for selected player in game.","3" ,2),
	ACCOUNT_CREATE("account create","Syntax: .account create $account $password [$expansion] Create account and set password to it. Optionally, you may also set another expansion for this account than the defined default value.","4" ,3),
	ACCOUNT_DELETE("account delete","Syntax: .account delete $account Delete account with all characters.","4" ,4),
	ACCOUNT_LOCK("account lock","Syntax: .account lock [on|off] Allow login from account only from current used IP or remove this requirement.","0" ,5),
	ACCOUNT_ONLINELIST("account onlinelist","Syntax: .account onlinelist Show list of online accounts.","4" ,6),
	ACCOUNT_PASSWORD("account password","Syntax: .account password $old_password $new_password $new_password Change your account password.","0" ,7),
	ACCOUNT_SET_ADDON("account set addon","Syntax: .account set addon [#accountId|$accountName] #addon Set user (possible targeted  expansion addon level allowed. Addon values: 0 - normal, 1 - tbc, 2 - wotlk.","3" ,8),
	ACCOUNT_SET_GMLEVEL("account set gmlevel","Syntax: .account set gmlevel [#accountId|$accountName] #level Set the security level for targeted player (can't be used at self  or for #accountId or $accountName to a level of #level. #level may range from 0 to 3.","4" ,9),
	ACCOUNT_SET_PASSWORD("account set password","Syntax: .account set password (#accountId|$accountName  $password $password Set password for account.","4" ,10),
	ACHIEVEMENT("achievement","Syntax: .achievement $playername #achivementid Show state achievment #achivmentid (can be shift link  and list of achievement criteria with progress data for selected player in game or by player name.","3" ,11),
	ACHIEVEMENT_ADD("achievement add","Syntax: .achievement add $playername #achivementid Complete achievement and all it's criteria for selected player in game or by player name. Command can't be used for counter achievements.","3" ,12),
	ACHIEVEMENT_CRITERIA_ADD("achievement criteria add","Syntax: .achievement criteria add $playername #criteriaid #change Increase progress for non-completed criteria at #change for selected player in game or by player name. If #chnage not provided then non-counter criteria progress set to completed state. For counter criteria increased at 1.","3" ,13),
	ACHIEVEMENT_CRITERIA_REMOVE("achievement criteria remove","Syntax: .achievement criteria remove $playername #criteriaid #change ecrease progress for criteria at #change for selected player in game or by player name. If #chnage not provided then criteria progress reset to 0.","3" ,14),
	ACHIEVEMENT_REMOVE("achievement remove","Syntax: .achievement remove $playername #achivementid Remove complete state for achievement #achivmentid and reset all achievement's criteria for selected player in game or by player name. Also command can be used for reset counter achievements.","3" ,15),
	GO_OBJECT("go object","Syntax: .go object (#gameobject_guid|$gameobject_name|id #gameobject_id  Teleport your character to gameobject with guid #gameobject_guid, or teleport your character to gameobject with name including as part $gameobject_name substring, or teleport your character to a gameobject that was spawned from the template with this entry #gameobject_id.","1" ,16),
	ADDITEMSET("additemset","Syntax: .additemset #itemsetid Add items from itemset of id #itemsetid to your or selected character inventory. Will add by one example each item from itemset.","3" ,17),
	AHBOT_ITEMS_AMOUNT("ahbot items amount","Syntax: .ahbot items amount $GreyItems $WhiteItems $GreenItems $BlueItems $PurpleItems $OrangeItems $YellowItems Set amount of each items color be selled on auction.","3" ,18),
	AHBOT_ITEMS_AMOUNT_BLUE("ahbot items amount blue","Syntax: .ahbot items amount blue $BlueItems Set amount of Blue color items be selled on auction.","3" ,19),
	AHBOT_ITEMS_AMOUNT_GREEN("ahbot items amount green","Syntax: .ahbot items amount green $GreenItems Set amount of Green color items be selled on auction.","3" ,20),
	AHBOT_ITEMS_AMOUNT_GREY("ahbot items amount grey","Syntax: .ahbot items amount grey $GreyItems Set amount of Grey color items be selled on auction.","3" ,21),
	AHBOT_ITEMS_AMOUNT_ORANGE("ahbot items amount orange","Syntax: .ahbot items amount orange $OrangeItems Set amount of Orange color items be selled on auction.","3" ,22),
	AHBOT_ITEMS_AMOUNT_PURPLE("ahbot items amount purple","Syntax: .ahbot items amount purple $PurpleItems Set amount of Purple color items be selled on auction.","3" ,23),
	AHBOT_ITEMS_AMOUNT_WHITE("ahbot items amount white","Syntax: .ahbot items amount white $WhiteItems Set amount of White color items be selled on auction.","3" ,24),
	AHBOT_ITEMS_AMOUNT_YELLOW("ahbot items amount yellow","Syntax: .ahbot items amount yellow $YellowItems Set amount of Yellow color items be selled on auction.","3" ,25),
	AHBOT_ITEMS_RATIO("ahbot items ratio","Syntax: .ahbot items ratio $allianceratio $horderatio $neutralratio Set ratio of items in 3 auctions house.","3" ,26),
	AHBOT_ITEMS_RATIO_ALLIANCE("ahbot items ratio alliance","Syntax: .ahbot items ratio alliance $allianceratio Set ratio of items in alliance auction house.","3" ,27),
	AHBOT_ITEMS_RATIO_HORDE("ahbot items ratio horde","Syntax: .ahbot items ratio horde $horderatio Set ratio of items in horde auction house.","3" ,28),
	AHBOT_ITEMS_RATIO_NEUTRAL("ahbot items ratio neutral","Syntax: .ahbot items ratio neutral $neutralratio Set ratio of items in $neutral auction house.","3" ,29),
	AHBOT_REBUILD("ahbot rebuild","Syntax: .ahbot rebuild [all] Expire all actual auction of ahbot except bided by player. Binded auctions included to expire if 'all' option used. Ahbot re-fill auctions base at current settings then.","3" ,30),
	AHBOT_RELOAD("ahbot reload","Syntax: .ahbot reload Reload_AHBot settings from configuration file.","3" ,31),
	AHBOT_STATUS("ahbot status","Syntax: .ahbot status [all] Show current ahbot state data in short form, and with 'all' with details.","3" ,32),
	ANNOUNCE("announce","Syntax: .announce $MessageToBroadcast Send a global message to all players online in chat log.","1" ,33),
	GO_GRAVEYARD("go graveyard","Syntax: .go graveyard #graveyardId Teleport to graveyard with the graveyardId specified.","1" ,34),
	AUCTION("auction","Syntax: .auction Show your team auction store.","3" ,35),
	AUCTION_ALLIANCE("auction alliance","Syntax: .auction alliance Show alliance auction store independent from your team.","3" ,36),
	AUCTION_GOBLIN("auction goblin","Syntax: .auction goblin Show goblin auction store common for all teams.","3" ,37),
	AUCTION_HORDE("auction horde","Syntax: .auction horde Show horde auction store independent from your team.","3" ,38),
	AUCTION_ITEM("auction item","Syntax: .auction item (alliance|horde|goblin  #itemid[:#itemcount] [[[#minbid] #buyout] [short|long|verylong] Add new item (in many stackes if amount grater stack size  to specific auction house at short|long|verylogn perios similar same settings in auction in game dialog. Created auction not have owner.","3" ,39),
	AURA("aura","Syntax: .aura #spellid Add the aura from spell #spellid to the selected Unit.","3" ,40),
	GO_CREATURE("go creature","Syntax: .go creature (#creature_guid|$creature_name|id #creature_id  Teleport your character to creature with guid #creature_guid, or teleport your character to creature with name including as part $creature_name substring, or teleport your character to a creature that was spawned from the template with this entry #creature_id.","1" ,41),
	GO("go","Syntax: .go [$playername|pointlink|#x #y #z [#mapid]] Teleport your character to point with coordinates of player $playername, or coordinates of one from shift-link types: player, tele, taxinode, creature/creature_entry, gameobject/gameobject_entry, or explicit #x #y #z #mapid coordinates.","1" ,42),
	GM_FLY("gm fly","Syntax: .gm fly [on/off] Enable/disable gm fly mode.","3" ,43),
	BANK("bank","Syntax: .bank Show your bank inventory.","3" ,44),
	EXPLORECHEAT("explorecheat","Syntax: .explorecheat #flag Reveal or hide all maps for the selected player. If no player is selected, hide or reveal maps to you. Use a #flag of value 1 to reveal, use a #flag value of 0 to hide all maps.","3" ,45),
	EVENT_START("event start","Syntax: .event start #event_id Start event #event_id. Set start time for event to current moment (change not saved in DB .","2" ,46),
	EVENT_STOP("event stop","Syntax: .event stop #event_id Stop event #event_id. Set start time for event to time in past that make current moment is event stop time (change not saved in DB .","2" ,47),
	EVENT_LIST("event list","Syntax: .event list Show list of currently active events. Show list of all events","2" ,48),
	DEBUG_PLAY_CINEMATIC("debug play cinematic","Syntax: .debug play cinematic #cinematicid Play cinematic #cinematicid for you. You stay at place while your mind fly.","1" ,49),
	EVENT("event","Syntax: .event #event_id Show details about event with #event_id.","2" ,50),
	COMBATSTOP("combatstop","Syntax: .combatstop [$playername] Stop combat for selected character. If selected non-player then command applied to self. If $playername provided then attempt applied to online player $playername.","2" ,51),
	CHARACTER_ACHIEVEMENTS("character achievements","Syntax: .character achievements [$player_name] Show completed achievments for selected player or player find by $player_name.","2" ,52),
	CHARACTER_CUSTOMIZE("character customize","Syntax: .character customize [$name] Mark selected in game or by $name in command character for customize at next login.","2" ,53),
	CHARACTER_DELETED_DELETE("character deleted delete","Syntax: .character deleted delete #guid|$name Completely deletes the selected characters. If $name is supplied, only characters with that string in their name will be deleted, if #guid is supplied, only the character with that_GUID will be deleted.","4" ,54),
	CHARACTER_DELETED_LIST("character deleted list","Syntax: .character deleted list [#guid|$name] Shows a list with all deleted characters. If $name is supplied, only characters with that string in their name will be selected, if #guid is supplied, only the character with that_GUID will be selected.","3" ,55),
	CHARACTER_DELETED_OLD("character deleted old","Syntax: .character deleted old [#keepDays] Completely deletes all characters with deleted time longer #keepDays. If #keepDays not provided the used value from mangosd.conf option 'CharDelete.KeepDays'. If referenced config option disabled (use 0 value  then command can't be used without #keepDays.","4" ,56),
	CHARACTER_DELETED_RESTORE("character deleted restore","Syntax: .character deleted restore #guid|$name [$newname] [#new account] Restores deleted characters. If $name is supplied, only characters with that string in their name will be restored, if $guid is supplied, only the character with that_GUID will be restored. If $newname is set, the character will be restored with that name instead of the original one. If #newaccount is set, the character will be restored to specific account character list. This works only with one character!","3" ,57),
	CHARACTER_ERASE("character erase","Syntax: .character erase $name Delete character $name. Character finally deleted in case any deleting options.","4" ,58),
	CHARACTER_LEVEL("character level","Syntax: .character level [$playername] [#level] Set the level of character with $playername (or the selected if not name provided  by #numberoflevels Or +1 if no #numberoflevels provided . If #numberoflevels is omitted, the level will be increase by 1. If #numberoflevels is 0, the same level will be restarted. If no character is selected and name not provided, increase your level. Command can be used for offline character. All stats and dependent values recalculated. At level decrease talents can be reset if need. Also at level decrease equipped items with greater level requirement can be lost.","3" ,59),
	CHARACTER_RENAME("character rename","Syntax: .character rename [$name] Mark selected in game or by $name in command character for rename at next login.","2" ,60),
	CHARACTER_REPUTATION("character reputation","Syntax: .character reputation [$player_name] Show reputation information for selected player or player find by $player_name.","2" ,61),
	CHARACTER_TITLES("character titles","Syntax: .character titles [$player_name] Show known titles list for selected player or player find by $player_name.","2" ,62),
	CAST_TARGET("cast target","Syntax: .cast target #spellid [triggered] Selected target will cast #spellid to his victim. If 'trigered' or part provided then spell casted with triggered flag.","3" ,63),
	COMMANDS("commands","Syntax: .commands Display a list of available commands for your account level.","0" ,64),
	COOLDOWN("cooldown","Syntax: .cooldown [#spell_id] Remove all (if spell_id not provided  or #spel_id spell cooldown from selected character or you (if no selection .","3" ,65),
	DAMAGE("damage","Syntax: .damage $damage_amount [$school [$spellid]] Apply $damage to target. If not $school and $spellid provided then this flat clean melee damage without any modifiers. If $school provided then damage modified by armor reduction (if school physical , and target absorbing modifiers and result applied as melee damage to target. If spell provided then damage modified and applied as spell damage. $spellid can be shift-link.","3" ,66),
	DEBUG_ANIM("debug anim","Syntax: .debug anim #emoteid Play emote #emoteid for your character.","2" ,67),
	DEBUG_ARENA("debug arena","Syntax: .debug arena Toggle debug mode for arenas. In debug mode GM can start arena with single player.","3" ,68),
	DEBUG_BG("debug bg","Syntax: .debug bg Toggle debug mode for battlegrounds. In debug mode GM can start battleground with single player.","3" ,69),
	DEBUG_GETITEMVALUE("debug getitemvalue","Syntax: .debug getitemvalue #itemguid #field [int|hex|bit|float] Get the field #field of the item #itemguid in your inventroy. Use type arg for set output format: int (decimal number , hex (hex value , bit (bitstring , float. By default use integer output.","3" ,70),
	DEBUG_GETVALUE("debug getvalue","Syntax: .debug getvalue #field [int|hex|bit|float] Get the field #field of the selected target. If no target is selected, get the content of your field. Use type arg for set output format: int (decimal number , hex (hex value , bit (bitstring , float. By default use integer output.","3" ,71),
	DEBUG_MODITEMVALUE("debug moditemvalue","Syntax: .debug moditemvalue #guid #field [int|float| &= | |= | &=~ ] #value Modify the field #field of the item #itemguid in your inventroy by value #value. Use type arg for set mode of modification: int (normal add/subtract #value as decimal number , float (add/subtract #value as float number , &= (bit and, set to 0 all bits in value if it not set to 1 in #value as hex number , |= (bit or, set to 1 all bits in value if it set to 1 in #value as hex number , &=~ (bit and not, set to 0 all bits in value if it set to 1 in #value as hex number . By default expect integer add/subtract.","3" ,72),
	DEBUG_MODVALUE("debug modvalue","Syntax: .debug modvalue #field [int|float| &= | |= | &=~ ] #value Modify the field #field of the selected target by value #value. If no target is selected, set the content of your field. Use type arg for set mode of modification: int (normal add/subtract #value as decimal number , float (add/subtract #value as float number , &= (bit and, set to 0 all bits in value if it not set to 1 in #value as hex number , |= (bit or, set to 1 all bits in value if it set to 1 in #value as hex number , &=~ (bit and not, set to 0 all bits in value if it set to 1 in #value as hex number . By default expect integer add/subtract.","3" ,73),
	CAST_SELF("cast self","Syntax: .cast self #spellid [triggered] Cast #spellid by target at target itself. If 'trigered' or part provided then spell casted with triggered flag.","3" ,74),
	DEBUG_PLAY_MOVIE("debug play movie","Syntax: .debug play movie #movieid Play movie #movieid for you.","1" ,75),
	DEBUG_PLAY_SOUND("debug play sound","Syntax: .debug play sound #soundid Play sound with #soundid. Sound will be play only for you. Other players do not hear this. Warning: client may have more 5000 sounds...","1" ,76),
	DEBUG_SETITEMVALUE("debug setitemvalue","Syntax: .debug setitemvalue #guid #field [int|hex|bit|float] #value Set the field #field of the item #itemguid in your inventroy to value #value. Use type arg for set input format: int (decimal number , hex (hex value , bit (bitstring , float. By default expect integer input format.","3" ,77),
	DEBUG_SETVALUE("debug setvalue","Syntax: .debug setvalue #field [int|hex|bit|float] #value Set the field #field of the selected target to value #value. If no target is selected, set the content of your field. Use type arg for set input format: int (decimal number , hex (hex value , bit (bitstring , float. By default expect integer input format.","3" ,78),
	DEBUG_SPELLCOEFS("debug spellcoefs","Syntax: .debug spellcoefs #spellid Show default calculated and DB stored coefficients for direct/dot heal/damage.","3" ,79),
	DEBUG_SPELLMODS("debug spellmods","Syntax: .debug spellmods (flat|pct  #spellMaskBitIndex #spellModOp #value Set at client side spellmod affect for spell that have bit set with index #spellMaskBitIndex in spell family mask for values dependent from spellmod #spellModOp to #value.","3" ,80),
	DELTICKET("delticket","Syntax: .delticket all .delticket #num .delticket $character_name all to dalete all tickets at server, $character_name to delete ticket of this character, #num to delete ticket #num.","2" ,81),
	DEMORPH("demorph","Syntax: .demorph Demorph the selected player.","2" ,82),
	DIE("die","Syntax: .die Kill the selected player. If no player is selected, it will kill you.","3" ,83),
	DISMOUNT("dismount","Syntax: .dismount Dismount you, if you are mounted.","0" ,84),
	DISTANCE("distance","Syntax: .distance [$name/$link] Display the distance from your character to the selected creature/player, or player with name $name, or player/creature/gameobject pointed to shift-link with guid.","3" ,85),
	CAST_DIST("cast dist","Syntax: .cast dist #spellid [#dist [triggered]] You will cast spell to pint at distance #dist. If 'trigered' or part provided then spell casted with triggered flag. Not all spells can be casted as area spells.","3" ,86),
	CAST_BACK("cast back","Syntax: .cast back #spellid [triggered] Selected target will cast #spellid to your character. If 'trigered' or part provided then spell casted with triggered flag.","3" ,87),
	BANLIST_IP("banlist ip","Syntax: .banlist ip [$Ip] Searches the banlist for a IP pattern or show full list of IP bans.","3" ,88),
	CAST("cast","Syntax: .cast #spellid [triggered] Cast #spellid to selected target. If no target selected cast to self. If 'trigered' or part provided then spell casted with triggered flag.","3" ,89),
	FLUSHARENAPOINTS("flusharenapoints","Syntax: .flusharenapoints Use it to distribute arena points based on arena team ratings, and start a new week.","3" ,90),
	GEARSCORE("gearscore","Syntax: .gearscore [#withBags] [#withBank] Show selected player's gear score. Check items in bags if #withBags != 0 and check items in Bank if #withBank != 0. Default: 1 for bags and 0 for bank","3" ,91),
	GM("gm","Syntax: .gm [on/off] Enable or Disable in game GM_MODE or show current state of on/off not provided.","1" ,92),
	GM_CHAT("gm chat","Syntax: .gm chat [on/off] Enable or disable chat GM_MODE (show gm badge in messages  or show current state of on/off not provided.","1" ,93),
	GM_INGAME("gm ingame","Syntax: .gm ingame Display a list of available in game Game Masters.","0" ,94),
	GM_LIST("gm list","Syntax: .gm list Display a list of all Game Masters accounts and security levels.","3" ,95),
	GM_SETVIEW("gm setview","Syntax: .gm setview Set farsight view on selected unit. Select yourself to set view back.","1" ,96),
	GM_VISIBLE("gm visible","Syntax: .gm visible on/off Output current visibility state or make GM visible(on  and invisible(off  for other players.","1" ,97),
	BANLIST_ACCOUNT("banlist account","Syntax: .banlist account [$Name] Searches the banlist for a account name pattern or show full list account bans.","3" ,98),
	BANLIST_CHARACTER("banlist character","Syntax: .banlist character $Name Searches the banlist for a character name pattern. Pattern required.","3" ,99),
	BANINFO_ACCOUNT("baninfo account","Syntax: .baninfo account $accountid Watch full information about a specific ban.","3" ,100),
	BANINFO_CHARACTER("baninfo character","Syntax: .baninfo character $charactername Watch full information about a specific ban.","3" ,101),
	BANINFO_IP("baninfo ip","Syntax: .baninfo ip $ip Watch full information about a specific ban.","3" ,102),
	GO_GRID("go grid","Syntax: .go grid #gridX #gridY [#mapId] Teleport the gm to center of grid with provided indexes at map #mapId (or current map if it not provided .","1" ,103),
	BAN_CHARACTER("ban character","Syntax: .ban character $Name $bantime $reason Ban account and kick player. $bantime: negative value leads to permban, otherwise use a timestring like '4d20h3s.","3" ,104),
	BAN_IP("ban ip","Syntax: .ban ip $Ip $bantime $reason Ban IP. $bantime: negative value leads to permban, otherwise use a timestring like 4d20h3s","3" ,105),
	GO_TAXINODE("go taxinode","Syntax: .go taxinode #taxinode Teleport player to taxinode coordinates. You can look up zone using .lookup taxinode $namepart","1" ,106),
	APPEAR("appear","Syntax: .appear [$charactername] Teleport to the given character. Either specify the character name or click on the character's portrait, e.g. when you are in a group. Character can be offline.","1" ,107),
	BAN_ACCOUNT("ban account","Syntax: .ban account $Name $bantime $reason Ban account kick player. $bantime: negative value leads to permban, otherwise use a timestring like '4d20h3s'","3" ,108),
	GO_XY("go xy","Syntax: .go xy #x #y [#mapid] Teleport player to point with (#x,#y  coordinates at ground(water  level at map #mapid or same map if #mapid not provided.","1" ,109),
	GO_XYZ("go xyz","Syntax: .go xyz #x #y #z [#mapid] Teleport player to point with (#x,#y,#z  coordinates at ground(water  level at map #mapid or same map if #mapid not provided.","1" ,110),
	GO_ZONEXY("go zonexy","Syntax: .go zonexy #x #y [#zone] Teleport player to point with (#x,#y  client coordinates at ground(water  level in zone #zoneid or current zone if #zoneid not provided. You can look up zone using .lookup area $namepart","1" ,111),
	ADDITEM("additem","Syntax: .additem #itemid/[#itemname]/#shift-click-item-link #itemcount Adds the specified number of items of id #itemid (or exact (!  name $itemname in brackets, or link created by shift-click at item in inventory or recipe  to your or selected character inventory. If #itemcount is omitted, only one item will be added.","3" ,112),
	GOBJECT_MOVE("gobject move","Syntax: .gobject move #goguid [#x #y #z] Move gameobject #goguid to character coordinates (or to (#x,#y,#z  coordinates if its provide .","2" ,113),
	GOBJECT_NEAR("gobject near","Syntax: .gobject near [#distance] Output gameobjects at distance #distance from player. Output gameobject guids and coordinates sorted by distance from character. If #distance not provided use 10 as default value.","2" ,114),
	GOBJECT_SETPHASE("gobject setphase","Syntax: .gobject setphase #guid #phasemask Gameobject with DB guid #guid phasemask changed to #phasemask with related world vision update for players. Gameobject state saved to DB and persistent.","2" ,115),
	GOBJECT_TARGET("gobject target","Syntax: .gobject target [#go_id|#go_name_part] Locate and show position nearest gameobject. If #go_id or #go_name_part provide then locate and show position of nearest gameobject with gameobject template id #go_id or name included #go_name_part as part.","2" ,116),
	NPC_ADDITEM("npc additem","Syntax: .npc additem #itemId <#maxcount><#incrtime><#extendedcost> Add item #itemid to item list of selected vendor. Also optionally set max count item in vendor item list and time to item count restoring and items ExtendedCost.","2" ,117),
	GPS("gps","Syntax: .gps [$name|$shift-link] Display the position information for a selected character or creature (also if player name $name provided then for named player, or if creature/gameobject shift-link provided then pointed creature/gameobject if it loaded . Position information includes X, Y, Z, and orientation, map Id and zone Id","1" ,118),
	GROUPGO("groupgo","Syntax: .groupgo [$charactername] Teleport the given character and his group to you. Teleported only online characters but original selected group member can be offline.","1" ,119),
	GUID("guid","Syntax: .guid Display the_GUID for the selected character.","2" ,120),
	GUILD_CREATE("guild create","Syntax: .guild create [$GuildLeaderName] '$GuildName' Create a guild named $GuildName with the player $GuildLeaderName (or selected  as leader. Guild name must in quotes.","2" ,121),
	GUILD_DELETE("guild delete","Syntax: .guild delete '$GuildName' Delete guild $GuildName. Guild name must in quotes.","2" ,122),
	GUILD_INVITE("guild invite","Syntax: .guild invite [$CharacterName] '$GuildName' Add player $CharacterName (or selected  into a guild $GuildName. Guild name must in quotes.","2" ,123),
	GUILD_RANK("guild rank","Syntax: .guild rank [$CharacterName] #Rank Set for player $CharacterName (or selected  rank #Rank in a guild.","2" ,124),
	GUILD_UNINVITE("guild uninvite","Syntax: .guild uninvite [$CharacterName] Remove player $CharacterName (or selected  from a guild.","2" ,125),
	HELP("help","Syntax: .help [$command] Display usage instructions for the given $command. If no $command provided show list available commands.","0" ,126),
	HIDEAREA("hidearea","Syntax: .hidearea #areaid Hide the area of #areaid to the selected character. If no character is selected, hide this area to you.","3" ,127),
	HONOR_ADD("honor add","Syntax: .honor add $amount Add a certain amount of honor (gained today  to the selected player.","2" ,128),
	HONOR_ADDKILL("honor addkill","Syntax: .honor addkill Add the targeted unit as one of your pvp kills today (you only get honor if it's a racial leader or a player ","2" ,129),
	HONOR_UPDATE("honor update","Syntax: .honor update Force the yesterday's honor fields to be updated with today's data, which will get reset for the selected player.","2" ,130),
	MOVEGENS("movegens","Syntax: .movegens Show movement generators stack for selected creature or player.","3" ,131),
	MODIFY_SWIM("modify swim","Syntax: .modify swim #rate Modify the swim speed of the selected player to 'normal swim speed'*rate. If no player is selected, modify your speed. #rate may range from 0.1 to GM.MaxSpeedFactor defined in mangosd.conf.","1" ,132),
	ITEMMOVE("itemmove","Syntax: .itemmove #sourceslotid #destinationslotid Move an item from slots #sourceslotid to #destinationslotid in your inventory Not yet implemented","2" ,133),
	KICK("kick","Syntax: .kick [$charactername] Kick the given character name from the world. If no character name is provided then the selected player (except for yourself  will be kicked.","2" ,134),
	LEARN("learn","Syntax: .learn #spell [all] Selected character learn a spell of id #spell. If 'all' provided then all ranks learned.","3" ,135),
	LEARN_ALL_CRAFTS("learn all_crafts","Syntax: .learn crafts Learn all professions and recipes.","2" ,136),
	LEARN_ALL_DEFAULT("learn all_default","Syntax: .learn all_default [$playername] Learn for selected/$playername player all default spells for his race/class and spells rewarded by completed quests.","1" ,137),
	LEARN_ALL_GM("learn all_gm","Syntax: .learn all_gm Learn all default spells for Game Masters.","2" ,138),
	LEARN_ALL_LANG("learn all_lang","Syntax: .learn all_lang Learn all languages","1" ,139),
	LEARN_ALL_MYCLASS("learn all_myclass","Syntax: .learn all_myclass Learn all spells and talents available for his class.","3" ,140),
	LEARN_ALL_MYPETTALENTS("learn all_mypettalents","Syntax: .learn all_mypettalents Learn all talents for your pet available for his creature type (only for hunter pets .","3" ,141),
	LEARN_ALL_MYSPELLS("learn all_myspells","Syntax: .learn all_myspells Learn all spells (except talents and spells with first rank learned as talent  available for his class.","3" ,142),
	LEARN_ALL_MYTALENTS("learn all_mytalents","Syntax: .learn all_mytalents Learn all talents (and spells with first rank learned as talent  available for his class.","3" ,143),
	MODIFY_SPEED("modify speed","Syntax: .modify speed #rate Modify the running speed of the selected player to 'normal base run speed'*rate. If no player is selected, modify your speed. #rate may range from 0.1 to GM.MaxSpeedFactor defined in mangosd.conf.","1" ,144),
	LEVELUP("levelup","Syntax: .levelup [$playername] [#numberoflevels] Increase/decrease the level of character with $playername (or the selected if not name provided  by #numberoflevels Or +1 if no #numberoflevels provided . If #numberoflevels is omitted, the level will be increase by 1. If #numberoflevels is 0, the same level will be restarted. If no character is selected and name not provided, increase your level. Command can be used for offline character. All stats and dependent values recalculated. At level decrease talents can be reset if need. Also at level decrease equipped items with greater level requirement can be lost.","3" ,145),
	LINKGRAVE("linkgrave","Syntax: .linkgrave #graveyard_id [alliance|horde] Link current zone to graveyard for any (or alliance/horde faction ghosts . This let character ghost from zone teleport to graveyard after die if graveyard is nearest from linked to zone and accept ghost of this faction. Add only single graveyard at another map and only if no graveyards linked (or planned linked at same map .","3" ,146),
	LIST_CREATURE("list creature","Syntax: .list creature #creature_id [#max_count] Output creatures with creature id #creature_id found in world. Output creature guids and coordinates sorted by distance from character. Will be output maximum #max_count creatures. If #max_count not provided use 10 as default value.","3" ,147),
	LIST_ITEM("list item","Syntax: .list item #item_id [#max_count] Output items with item id #item_id found in all character inventories, mails, auctions, and guild banks. Output item guids, item owner guid, owner account and owner name (guild name and guid in case guild bank . Will be output maximum #max_count items. If #max_count not provided use 10 as default value.","3" ,148),
	LIST_OBJECT("list object","Syntax: .list object #gameobject_id [#max_count] Output gameobjects with gameobject id #gameobject_id found in world. Output gameobject guids and coordinates sorted by distance from character. Will be output maximum #max_count gameobject. If #max_count not provided use 10 as default value.","3" ,149),
	LIST_TALENTS("list talents","Syntax: .list talents Show list all really known (as learned spells  talent rank spells for selected player or self.","3" ,150),
	LOADSCRIPTS("loadscripts","Syntax: .loadscripts $scriptlibraryname Unload current and load the script library $scriptlibraryname or reload current if $scriptlibraryname omitted, in case you changed it while the server was running.","3" ,151),
	LOOKUP_ACCOUNT_EMAIL("lookup account email","Syntax: .lookup account email $emailpart [#limit] Searchs accounts, which email including $emailpart with optional parametr #limit of results. If #limit not provided expected 100.","2" ,152),
	LOOKUP_ACCOUNT_IP("lookup account ip","Syntax: lookup account ip $ippart [#limit] Searchs accounts, which last used ip inluding $ippart (textual  with optional parametr #$limit of results. If #limit not provided expected 100.","2" ,153),
	LOOKUP_ACCOUNT_NAME("lookup account name","Syntax: .lookup account name $accountpart [#limit] Searchs accounts, which username including $accountpart with optional parametr #limit of results. If #limit not provided expected 100.","2" ,154),
	LOOKUP_ACHIEVEMENT("lookup achievement","Syntax: .lookup $name Looks up a achievement by $namepart, and returns all matches with their quest ID's. Achievement shift-links generated with information about achievment state for selected player. Also for completed achievments in list show complete date.","2" ,155),
	LOOKUP_AREA("lookup area","Syntax: .lookup area $namepart Looks up an area by $namepart, and returns all matches with their area ID's.","1" ,156),
	LOOKUP_CREATURE("lookup creature","Syntax: .lookup creature $namepart Looks up a creature by $namepart, and returns all matches with their creature ID's.","3" ,157),
	LOOKUP_ITEM("lookup item","Syntax: .lookup item $itemname Looks up an item by $itemname, and returns all matches with their Item ID's.","3" ,158),
	LOOKUP_ITEMSET("lookup itemset","Syntax: .lookup itemset $itemname Looks up an item set by $itemname, and returns all matches with their Item set ID's.","3" ,159),
	LOOKUP_OBJECT("lookup object","Syntax: .lookup object $objname Looks up an gameobject by $objname, and returns all matches with their Gameobject ID's.","3" ,160),
	LOOKUP_PLAYER_ACCOUNT("lookup player account","Syntax: .lookup player account $accountpart [#limit] Searchs players, which account username including $accountpart with optional parametr #limit of results. If #limit not provided expected 100.","2" ,161),
	LOOKUP_PLAYER_EMAIL("lookup player email","Syntax: .lookup player email $emailpart [#limit] Searchs players, which account email including $emailpart with optional parametr #limit of results. If #limit not provided expected 100.","2" ,162),
	LOOKUP_PLAYER_IP("lookup player ip","Syntax: .lookup player ip $ippart [#limit] Searchs players, which account last used ip inluding $ippart (textual  with optional parametr #limit of results. If #limit not provided expected 100.","2" ,163),
	LOOKUP_POOL("lookup pool","Syntax: .lookup pool $pooldescpart List of pools (anywhere  with substring in description.","2" ,164),
	LOOKUP_QUEST("lookup quest","Syntax: .lookup quest $namepart Looks up a quest by $namepart, and returns all matches with their quest ID's.","3" ,165),
	LOOKUP_SKILL("lookup skill","Syntax: .lookup skill $$namepart Looks up a skill by $namepart, and returns all matches with their skill ID's.","3" ,166),
	LOOKUP_SPELL("lookup spell","Syntax: .lookup spell $namepart Looks up a spell by $namepart, and returns all matches with their spell ID's.","3" ,167),
	LOOKUP_TAXINODE("lookup taxinode","Syntax: .lookup taxinode $substring Search and output all taxinodes with provide $substring in name.","3" ,168),
	LOOKUP_TELE("lookup tele","Syntax: .lookup tele $substring Search and output all .tele command locations with provide $substring in name.","1" ,169),
	LOOKUP_TITLE("lookup title","Syntax: .lookup title $$namepart Looks up a title by $namepart, and returns all matches with their title ID's and index's.","2" ,170),
	MAILBOX("mailbox","Syntax: .mailbox Show your mailbox content.","3" ,171),
	MODIFY_REP("modify rep","Syntax: .modify rep #repId (#repvalue | $rankname [#delta]  Sets the selected players reputation with faction #repId to #repvalue or to $reprank. If the reputation rank name is provided, the resulting reputation will be the lowest reputation for that rank plus the delta amount, if specified. You can use '.pinfo rep' to list all known reputation ids, or use '.lookup faction $name' to locate a specific faction id.","2" ,172),
	MODIFY_ARENA("modify arena","Syntax: .modify arena #value Add $amount arena points to the selected player.","1" ,173),
	MODIFY_MOUNT("modify mount","Syntax: .modify mount #id #speed Display selected player as mounted at #id creature and set speed to #speed value.","1" ,174),
	MODIFY_DRUNK("modify drunk","Syntax: .modify drunk #value Set drunk level to #value (0..100 . Value 0 remove drunk state, 100 is max drunked state.","1" ,175),
	MODIFY_MONEY("modify money","Syntax: .modify money #money Add or remove money to the selected player. If no player is selected, modify your money. #gold can be negative to remove money.","1" ,176),
	MODIFY_ENERGY("modify energy","Syntax: .modify energy #energy Modify the energy of the selected player. If no player is selected, modify your energy.","1" ,177),
	MODIFY_FACTION("modify faction","Syntax: .modify faction #factionid #flagid #npcflagid #dynamicflagid Modify the faction and flags of the selected creature. Without arguments, display the faction and flags of the selected creature.","1" ,178),
	MODIFY_FLY("modify fly","Syntax: .modify fly #rate .fly #rate Modify the flying speed of the selected player to 'normal base fly speed'*rate. If no player is selected, modify your fly. #rate may range from 0.1 to 10.","1" ,179),
	MODIFY_GENDER("modify gender","Syntax: .modify gender male/female Change gender of selected player.","2" ,180),
	MODIFY_HONOR("modify honor","Syntax: .modify honor $amount Add $amount honor points to the selected player.","1" ,181),
	MODIFY_HP("modify hp","Syntax: .modify hp #newhp Modify the hp of the selected player. If no player is selected, modify your hp.","1" ,182),
	MODIFY_MANA("modify mana","Syntax: .modify mana #newmana Modify the mana of the selected player. If no player is selected, modify your mana.","1" ,183),
	MODIFY_BWALK("modify bwalk","Syntax: .modify bwalk #rate Modify the speed of the selected player while running backwards to 'normal walk back speed'*rate. If no player is selected, modify your speed. #rate may range from 0.1 to GM.MaxSpeedFactor defined in mangosd.conf.","1" ,184),
	MODIFY_MORPH("modify morph","Syntax: .modify morph #displayid Change your current model id to #displayid.","2" ,185),
	MODIFY_PHASE("modify phase","Syntax: .modify phase #phasemask Selected character phasemask changed to #phasemask with related world vision update. Change active until in game phase changed, or GM-mode enable/disable, or re-login. Character pts pasemask update to same value.","3" ,186),
	MODIFY_RAGE("modify rage","Syntax: .modify rage #newrage Modify the rage of the selected player. If no player is selected, modify your rage.","1" ,187),
	LOOKUP_FACTION("lookup faction","Syntax: .lookup faction $name Attempts to find the ID of the faction with the provided $name.","3" ,188),
	MAXSKILL("maxskill","Syntax: .maxskill Sets all skills of the targeted player to their maximum values for its current level.","3" ,189),
	MODIFY_ASPEED("modify aspeed","Syntax: .modify aspeed #rate Modify all speeds -run,swim,run back,swim back- of the selected player to 'normalbase speed for this move type'*rate. If no player is selected, modify your speed. #rate may range from 0.1 to GM.MaxSpeedFactor defined in mangosd.conf.","1" ,190),
	MODIFY_RUNICPOWER("modify runicpower","Syntax: .modify runicpower #newrunicpower Modify the runic power of the selected player. If no player is selected, modify your runic power.","1" ,191),
	MODIFY_SCALE("modify scale","Syntax: .modify scale #scale Change model scale for targeted player (util relogin  or creature (until respawn .","1" ,192),
	LOOKUP_EVENT("lookup event","Syntax: .lookup event $name Attempts to find the ID of the event with the provided $name.","2" ,193),
	MODIFY_STANDSTATE("modify standstate","Syntax: .modify standstate #emoteid Change the emote of your character while standing to #emoteid.","2" ,194),
	LEARN_ALL("learn all","Syntax: .learn all Learn all big set different spells, maybe useful for Administrators.","3" ,195),
	LEARN_ALL_RECIPES("learn all_recipes","Syntax: .learn all_recipes [$profession] Learns all recipes of specified profession and sets skill level to max. Example: .learn all_recipes enchanting","2" ,196),
	MODIFY_TP("modify tp","Syntax: .modify tp #amount Ste free talent pointes for selected character or character's pet. It will be reset to default expected at next levelup/login/quest reward.","1" ,197),
	MUTE("mute","Syntax: .mute [$playerName] $timeInMinutes Disible chat messaging for any character from account of character $playerName (or currently selected  at $timeInMinutes minutes. Player can be offline.","1" ,198),
	NEARGRAVE("neargrave","Syntax: .neargrave [alliance|horde] Find nearest graveyard linked to zone (or only nearest from accepts alliance or horde faction ghosts .","3" ,199),
	NOTIFY("notify","Syntax: .notify $MessageToBroadcast Send a global message to all players online in screen.","1" ,200),
	NPC_ADD("npc add","Syntax: .npc add #creatureid Spawn a creature by the given template id of #creatureid.","2" ,201),
	INSTANCE_STATS("instance stats","Syntax: .instance stats Shows statistics about instances.","3" ,202),
	INSTANCE_UNBIND("instance unbind","Syntax: .instance unbind all All of the selected player's binds will be cleared. .instance unbind #mapid Only the specified #mapid instance will be cleared.","3" ,203),
	NPC_AIINFO("npc aiinfo","Syntax: .npc npc aiinfo Show npc AI and script information.","2" ,204),
	NPC_ALLOWMOVE("npc allowmove","Syntax: .npc allowmove Enable or disable movement creatures in world. Not implemented.","3" ,205),
	NPC_CHANGELEVEL("npc changelevel","Syntax: .npc changelevel #level Change the level of the selected creature to #level. #level may range from 1 to 63.","2" ,206),
	NPC_DELETE("npc delete","Syntax: .npc delete [#guid] Delete creature with guid #guid (or the selected if no guid is provided ","2" ,207),
	NPC_DELITEM("npc delitem","Syntax: .npc delitem #itemId Remove item #itemid from item list of selected vendor.","2" ,208),
	NPC_FACTIONID("npc factionid","Syntax: .npc factionid #factionid Set the faction of the selected creature to #factionid.","2" ,209),
	NPC_FLAG("npc flag","Syntax: .npc flag #npcflag Set the_NPC flags of creature template of the selected creature and selected creature to #npcflag._NPC flags will applied to all creatures of selected creature template after server restart or grid unload/load.","2" ,210),
	NPC_FOLLOW("npc follow","Syntax: .npc follow Selected creature start follow you until death/fight/etc.","2" ,211),
	INSTANCE_LISTBINDS("instance listbinds","Syntax: .instance listbinds Lists the binds of the selected player.","3" ,212),
	INSTANCE_SAVEDATA("instance savedata","Syntax: .instance savedata Save the InstanceData for the current player's map to the DB.","3" ,213),
	NPC_MOVE("npc move","Syntax: .npc move [#creature_guid] Move the targeted creature spawn point to your coordinates.","2" ,214),
	NPC_NAME("npc name","Syntax: .npc name $name Change the name of the selected creature or character to $name. Command disabled.","2" ,215),
	NPC_PLAYEMOTE("npc playemote","Syntax: .npc playemote #emoteid Make the selected creature emote with an emote of id #emoteid.","3" ,216),
	NPC_SETDEATHSTATE("npc setdeathstate","Syntax: .npc setdeathstate on/off Set default death state (dead/alive  for npc at spawn.","2" ,217),
	NPC_SETMODEL("npc setmodel","Syntax: .npc setmodel #displayid Change the model id of the selected creature to #displayid.","2" ,218),
	NPC_SETMOVETYPE("npc setmovetype","Syntax: .npc setmovetype [#creature_guid] stay/random/way [NODEL] Set for creature pointed by #creature_guid (or selected if #creature_guid not provided  movement type and move it to respawn position (if creature alive . Any existing waypoints for creature will be removed from the database if you do not use_NODEL. If the creature is dead then movement type will applied at creature respawn. Make sure you use_NODEL, if you want to keep the waypoints.","2" ,219),
	NPC_SETPHASE("npc setphase","Syntax: .npc setphase #phasemask Selected unit or pet phasemask changed to #phasemask with related world vision update for players. In creature case state saved to DB and persistent. In pet case change active until in game phase changed for owner, owner re-login, or GM-mode enable/disable..","2" ,220),
	NPC_SPAWNDIST("npc spawndist","Syntax: .npc spawndist #dist Adjust spawndistance of selected creature to dist.","2" ,221),
	GOBJECT_TURN("gobject turn","Syntax: .gobject turn #goguid Rotates gameobject #goguid around z-azis to match player's current orientation. Optional parameters are (#angle, #x,#y, #z  - floats that represent a rotation af angle #angle around direction (#x, #y, #z  i.e. a rotation quaternion.","2" ,222),
	NPC_SUBNAME("npc subname","Syntax: .npc subname $Name Change the subname of the selected creature or player to $Name. Command disabled.","2" ,223),
	NPC_TAME("npc tame","Syntax: .npc tame Tame selected creature (tameable non pet creature . You don't must have pet.","2" ,224),
	NPC_TEXTEMOTE("npc textemote","Syntax: .npc textemote #emoteid Make the selected creature to do textemote with an emote of id #emoteid.","1" ,225),
	NPC_UNFOLLOW("npc unfollow","Syntax: .npc unfollow Selected creature (non pet  stop follow you.","2" ,226),
	PINFO("pinfo","Syntax: .pinfo [$player_name] Output account information for selected player or player find by $player_name.","2" ,227),
	POOL("pool","Syntax: .pool #pool_id Pool information and full list creatures/gameobjects included in pool.","2" ,228),
	POOL_LIST("pool list","Syntax: .pool list List of pools with spawn in current map (only work in instances. Non-instanceable maps share pool system state os useless attempt get all pols at all continents.","2" ,229),
	POOL_SPAWNS("pool spawns","Syntax: .pool spawns #pool_id List current creatures/objects listed in pools (or in specific #pool_id  and spawned (added to grid data, not meaning show in world.","2" ,230),
	QUEST_ADD("quest add","Syntax: .quest add #quest_id Add to character quest log quest #quest_id. Quest started from item can't be added by this command but correct .additem call provided in command output.","3" ,231),
	WHISPERS("whispers","Syntax: .whispers on|off Enable/disable accepting whispers by GM from players. By default use mangosd.conf setting.","1" ,232),
	WP_MODIFY("wp modify","Syntax: .wp modify command [dbGuid, id] [value] where command must be one of: waittime | scriptid | orientation | del | move If no waypoint was selected, one can be chosen with dbGuid and id. The commands have the following meaning: waittime (Set the time the npc will wait at a point (in ms   scriptid (Set the DB-Script that will be executed when the wp is reached  orientation (Set the orientation of this point  del (Remove the waypoint from the path  move (Move the wayoint to the current position of the player ","2" ,233),
	QUEST_REMOVE("quest remove","Syntax: .quest remove #quest_id Set quest #quest_id state to not completed and not active (and remove from active quest list  for selected player.","3" ,234),
	QUIT("quit","Syntax: quit Close RA connection. Command must be typed fully (quit .","4" ,235),
	RECALL("recall","Syntax: .recall [$playername] Teleport $playername or selected player to the place where he has been before last use of a teleportation command. If no $playername is entered and no player is selected, it will teleport you.","1" ,236),
	RELOAD_ALL("reload all","Syntax: .reload all Reload all tables with reload support added and that can be _safe_ reloaded.","3" ,237),
	RELOAD_ALL_ACHIEVEMENT("reload all_achievement","Syntax: .reload all_achievement Reload all `achievement_*` tables if reload support added for this table and this table can be _safe_ reloaded.","3" ,238),
	RELOAD_ALL_AREA("reload all_area","Syntax: .reload all_area Reload all `areatrigger_*` tables if reload support added for this table and this table can be _safe_ reloaded.","3" ,239),
	RELOAD_ALL_EVENTAI("reload all_eventai","Syntax: .reload all_eventai Reload `creature_ai_*` tables if reload support added for these tables and these tables can be _safe_ reloaded.","3" ,240),
	RELOAD_ALL_ITEM("reload all_item","Syntax: .reload all_item Reload `item_required_target`, `page_texts` and `item_enchantment_template` tables.","3" ,241),
	RELOAD_ALL_LOCALES("reload all_locales","Syntax: .reload all_locales Reload all `locales_*` tables with reload support added and that can be _safe_ reloaded.","3" ,242),
	RELOAD_ALL_LOOT("reload all_loot","Syntax: .reload all_loot Reload all `*_loot_template` tables. This can be slow operation with lags for server run.","3" ,243),
	RELOAD_ALL_NPC("reload all_npc","Syntax: .reload all_npc Reload `points_of_interest` and `npc_*` tables if reload support added for these tables and these tables can be _safe_ reloaded.","3" ,244),
	RELOAD_ALL_QUEST("reload all_quest","Syntax: .reload all_quest Reload all quest related tables if reload support added for this table and this table can be _safe_ reloaded.","3" ,245),
	RELOAD_ALL_SCRIPTS("reload all_scripts","Syntax: .reload all_scripts Reload `dbscripts_on_*` tables.","3" ,246),
	RELOAD_CONFIG("reload config","Syntax: .reload config Reload config settings (by default stored in mangosd.conf . Not all settings can be change at reload: some new setting values will be ignored until restart, some values will applied with delay or only to new objects/maps, some values will explicitly rejected to change at reload.","3" ,247),
	REPAIRITEMS("repairitems","Syntax: .repairitems Repair all selected player's items.","2" ,248),
	RESET_ACHIEVEMENTS("reset achievements","Syntax: .reset achievements [$playername] Reset achievements data for selected or named (online or offline  character. Achievements for persistance progress data like completed quests/etc re-filled at reset. Achievements for events like kills/casts/etc will lost.","3" ,249),
	UNLEARN("unlearn","Syntax: .unlearn #spell [all] Unlearn for selected player a spell #spell. If 'all' provided then all ranks unlearned.","3" ,250),
	UNMUTE("unmute","Syntax: .unmute [$playerName] Restore chat messaging for any character from account of character $playerName (or selected . Character can be offline.","1" ,251),
	UNBAN_IP("unban ip","Syntax: .unban ip $Ip Unban accounts for IP pattern.","3" ,252),
	UNBAN_CHARACTER("unban character","Syntax: .unban character $Name Unban accounts for character name pattern.","3" ,253),
	RESET_SPECS("reset specs","Syntax: .reset specs [Playername] Removes all talents (for all specs  of the targeted player or named player. Playername can be name of offline character. With player talents also will be reset talents for all character's pets if any.","3" ,254),
	UNBAN_ACCOUNT("unban account","Syntax: .unban account $Name Unban accounts for account name pattern.","3" ,255),
	TRIGGER_ACTIVE("trigger active","Syntax: .trigger active Show list of areatriggers with activation zone including current character position.","2" ,256),
	TITLES_REMOVE("titles remove","Syntax: .titles remove #title Remove title #title (id or shift-link  from known titles list for selected player.","2" ,257),
	RESPAWN("respawn","Syntax: .respawn Respawn selected creature or respawn all nearest creatures (if none selected  and GO without waiting respawn time expiration.","3" ,258),
	REVIVE("revive","Syntax: .revive Revive the selected player. If no player is selected, it will revive you.","3" ,259),
	SAVE("save","Syntax: .save Saves your character.","0" ,260),
	SAVEALL("saveall","Syntax: .saveall Save all characters in game.","1" ,261),
	SEND_ITEMS("send items","Syntax: .send items #playername '#subject' '#text' itemid1[:count1] itemid2[:count2] ... itemidN[:countN] Send a mail to a player. Subject and mail text must be in. If for itemid not provided related count values then expected 1, if count > max items in stack then items will be send in required amount stacks. All stacks amount in mail limited to 12.","3" ,262),
	SEND_MAIL("send mail","Syntax: .send mail #playername '#subject'' '#text'' Send a mail to a player. Subject and mail text must be in ","1" ,263),
	SEND_MASS_ITEMS("send mass items","Syntax: .send mass items #racemask|$racename|alliance|horde|all '#subject' '#text' itemid1[:count1] itemid2[:count2] ... itemidN[:countN] Send a mail to players. Subject and mail text must be in If for itemid not provided related count values then expected 1, if count > max items in stack then items will be send in required amount stacks. All stacks amount in mail limited to 12.","3" ,264),
	SEND_MASS_MAIL("send mass mail","Syntax: .send mass mail #racemask|$racename|alliance|horde|all '#subject' '#text'' Send a mail to players. Subject and mail text must be in ","3" ,265),
	SEND_MASS_MONEY("send mass money","Syntax: .send mass money #racemask|$racename|alliance|horde|all '#subject' '#text' #money Send mail with money to players. Subject and mail text must be in ","3" ,266),
	SEND_MESSAGE("send message","Syntax: .send message $playername $message Send screen message to player from_ADMINISTRATOR.","3" ,267),
	SEND_MONEY("send money","Syntax: .send money #playername '#subject' '#text' #money Send mail with money to a player. Subject and mail text must be in ","3" ,268),
	SERVER_CORPSES("server corpses","Syntax: .server corpses Triggering corpses expire check in world.","2" ,269),
	SERVER_EXIT("server exit","Syntax: .server exit Terminate mangosd_NOW. Exit code 0.","4" ,270),
	SERVER_IDLERESTART("server idlerestart","Syntax: .server idlerestart #delay Restart the server after #delay seconds if no active connections are present (no players . Use #exist_code or 2 as program exist code.","3" ,271),
	SERVER_IDLERESTART_CANCEL("server idlerestart cancel","Syntax: .server idlerestart cancel Cancel the restart/shutdown timer if any.","3" ,272),
	SERVER_IDLESHUTDOWN("server idleshutdown","Syntax: .server idleshutdown #delay [#exist_code] Shut the server down after #delay seconds if no active connections are present (no players . Use #exist_code or 0 as program exist code.","3" ,273),
	SERVER_IDLESHUTDOWN_CANCEL("server idleshutdown cancel","Syntax: .server idleshutdown cancel Cancel the restart/shutdown timer if any.","3" ,274),
	SERVER_INFO("server info","Syntax: .server info Display server version and the number of connected players.","0" ,275),
	SERVER_LOG_FILTER("server log filter","Syntax: .server log filter [($filtername|all  (on|off ] Show or set server log filters. If used 'all' then all filters will be set to on/off state.","4" ,276),
	SERVER_LOG_LEVEL("server log level","Syntax: .server log level [#level] Show or set server log level (0 - errors only, 1 - basic, 2 - detail, 3 - debug .","4" ,277),
	SERVER_MOTD("server motd","Syntax: .server motd Show server Message of the day.","0" ,278),
	SERVER_PLIMIT("server plimit","Syntax: .server plimit [#num|-1|-2|-3|reset|player|moderator|gamemaster|administrator] Without arg show current player amount and security level limitations for login to server, with arg set player linit ($num > 0  or securiti limitation ($num < 0 or security leme name. With `reset` sets player limit to the one in the config file","3" ,279),
	SERVER_RESTART("server restart","Syntax: .server restart #delay Restart the server after #delay seconds. Use #exist_code or 2 as program exist code.","3" ,280),
	SERVER_RESTART_CANCEL("server restart cancel","Syntax: .server restart cancel Cancel the restart/shutdown timer if any.","3" ,281),
	SERVER_SET_MOTD("server set motd","Syntax: .server set motd $MOTD Set server Message of the day.","3" ,282),
	SERVER_SHUTDOWN("server shutdown","Syntax: .server shutdown #delay [#exit_code] Shut the server down after #delay seconds. Use #exit_code or 0 as program exit code.","3" ,283),
	SERVER_SHUTDOWN_CANCEL("server shutdown cancel","Syntax: .server shutdown cancel Cancel the restart/shutdown timer if any.","3" ,284),
	SETSKILL("setskill","Syntax: .setskill #skill #level [#max] Set a skill of id #skill with a current skill value of #level and a maximum value of #max (or equal current maximum if not provide  for the selected character. If no character is selected, you learn the skill.","3" ,285),
	SHOWAREA("showarea","Syntax: .showarea #areaid Reveal the area of #areaid to the selected character. If no character is selected, reveal this area to you.","3" ,286),
	STABLE("stable","Syntax: .stable Show your pet stable.","3" ,287),
	START("start","Syntax: .start Teleport you to the starting area of your character.","0" ,288),
	SUMMON("summon","Syntax: .summon [$charactername] Teleport the given character to you. Character can be offline.","1" ,289),
	TAXICHEAT("taxicheat","Syntax: .taxicheat on/off Temporary grant access or remove to all taxi routes for the selected character. If no character is selected, hide or reveal all routes to you. Visited taxi nodes still accessible after removing access.","1" ,290),
	TELE("tele","Syntax: .tele #location Teleport player to a given location.","1" ,291),
	TELE_ADD("tele add","Syntax: .tele add $name Add current your position to .tele command target locations list with name $name.","3" ,292),
	TELE_DEL("tele del","Syntax: .tele del $name Remove location with name $name for .tele command locations list.","3" ,293),
	TELE_GROUP("tele group","Syntax: .tele group#location Teleport a selected player and his group members to a given location.","1" ,294),
	TELE_NAME("tele name","Syntax: .tele name [#playername] #location Teleport the given character to a given location. Character can be offline.","1" ,295),
	TICKET("ticket","Syntax: .ticket on .ticket off .ticket #num .ticket $character_name .ticket respond #num $response .ticket respond $character_name $response on/off for GMs to show or not a new ticket directly, $character_name to show ticket of this character, #num to show ticket #num.","2" ,296),
	TITLES_ADD("titles add","Syntax: .titles add #title Add title #title (id or shift-link  to known titles list for selected player.","2" ,297),
	TITLES_CURRENT("titles current","Syntax: .titles current #title Set title #title (id or shift-link  as current selected titl for selected player. If title not in known title list for player then it will be added to list.","2" ,298),
	TITLES_SETMASK("titles setmask","Syntax: .titles setmask #mask Allows user to use all titles from #mask. #mask=0 disables the title-choose-field","2" ,299),
	TRIGGER("trigger","Syntax: .trigger [#trigger_id|$trigger_shift-link|$trigger_target_shift-link] Show detail infor about areatrigger with id #trigger_id or trigger id associated with shift-link. If areatrigger id or shift-link not provided then selected nearest areatrigger at current map.","2" ,300),
	RESET_TALENTS("reset talents","Syntax: .reset talents [Playername] Removes all talents (current spec  of the targeted player or pet or named player. With player talents also will be reset talents for all character's pets if any.","3" ,301),
	TRIGGER_NEAR("trigger near","Syntax: .trigger near [#distance] Output areatriggers at distance #distance from player. If #distance not provided use 10 as default value.","2" ,302),
	UNAURA("unaura","Syntax: .unaura #spellid Remove aura due to spell #spellid from the selected Unit.","3" ,303),
	RESET_STATS("reset stats","Syntax: .reset stats [Playername] Resets(recalculate  all stats of the targeted player to their original values at current level.","3" ,304),
	RESET_SPELLS("reset spells","Syntax: .reset spells [Playername] Removes all non-original spells from spellbook. Playername can be name of offline character.","3" ,305),
	RESET_HONOR("reset honor","Syntax: .reset honor [Playername] Reset all honor data for targeted character.","3" ,306),
	RESET_LEVEL("reset level","Syntax: .reset level [Playername] Reset level to 1 including reset stats and talents. Equipped items with greater level requirement can be lost.","3" ,307),
	WATERWALK("waterwalk","Syntax: .waterwalk on/off Set on/off waterwalk state for selected player.","2" ,308),
	WCHANGE("wchange","Syntax: .wchange #weathertype #status Set current weather to #weathertype with an intensity of #status. #weathertype can be 1 for rain, 2 for snow, and 3 for sand. #status can be 0 for disabled, and 1 for enabled.","3" ,309),
	RESET_ALL("reset all","Syntax: .reset all Request reset spells,talents etc. at next login each existing character.","3" ,310),
	WP_ADD("wp add","Syntax: .wp add [Selected Creature or dbGuid] [pathId [wpOrigin] ]","2" ,311),
	WP_EXPORT("wp export","Syntax: .wp export [#creature_guid or Select a Creature] $filename","3" ,312),
	PDUMP_WRITE("pdump write","Syntax: .pdump write $filename $playerNameOrGUID Write character dump with name/guid $playerNameOrGUID to file $filename.","3" ,313),
	QUEST_COMPLETE("quest complete","Syntax: .quest complete #questid Mark all quest objectives as completed for target character active quest. After this target character can go and get quest reward.","3" ,314),
	RELOAD_ALL_SPELL("reload all_spell","Syntax: .reload all_spell Reload all `spell_*` tables with reload support added and that can be _safe_ reloaded.","3" ,315),
	NPC_WHISPER("npc whisper","Syntax: .npc whisper #playerguid #text Make the selected npc whisper #text to #playerguid.","1" ,316),
	NPC_YELL("npc yell","Syntax: .npc yell #text Make the selected npc yells #text.","1" ,317),
	PDUMP_LOAD("pdump load","Syntax: .pdump load $filename $account [$newname] [$newguid] Load character dump from dump file into character list of $account with saved or $newname, with saved (or first free  or $newguid guid.","3" ,318),
	GO_TRIGGER("go trigger","Syntax: .go trigger (#trigger_id|$trigger_shift-link|$trigger_target_shift-link  [target] Teleport your character to areatrigger with id #trigger_id or trigger id associated with shift-link. If additional arg 'target' provided then character will teleported to areatrigger target point.","1" ,319),
	GOBJECT_ADD("gobject add","Syntax: .gobject add #id Add a game object from game object templates to the world at your current location using the #id. spawntimesecs sets the spawntime, it is optional. Note: this is a copy of .gameobject.","2" ,320),
	GOBJECT_DELETE("gobject delete","Syntax: .gobject delete #go_guid Delete gameobject with guid #go_guid.","2" ,321),
	NPC_INFO("npc info","Syntax: .npc info Display a list of details for the selected creature. The list includes:-_GUID, Faction,_NPC flags, Entry ID, Model ID, - Level, - Health (current/maximum , - Field flags, dynamic flags, faction template, - Position information, - and the creature type, e.g. if the creature is a vendor.","3" ,322),
	NPC_SAY("npc say","Syntax: .npc say #text Make the selected npc says #text.","1" ,323),
	NPC_SPAWNTIME("npc spawntime","Syntax: .npc spawntime #time Adjust spawntime of selected creature to time.","2" ,324),
	WP_SHOW("wp show","Syntax: .wp show command [dbGuid] [pathId [wpOrigin] where command can have one of the following values on (to show all related wp  first (to see only first one  last (to see only last one  off (to hide all related wp  info (to get more info about theses wp  For using info you have to do first show on and than select a Visual-Waypoint and do the show info! with pathId and wpOrigin you can specify which path to show (optional ","2" ,325);
	
	private String command;
	private String syntax;
	private String gmLevel;
	private int id;
	
	private Commands(String command, String syntax, String gmLevel, int id){
		this.command = command;
		this.syntax = syntax;
		this.gmLevel = gmLevel;
		this.id = id;
		
	}
	
	public String getCommand(){
		return this.command;
	}
	
	public String getSyntax(){
		return this.syntax;
	}
	
	public String getGmLevel(){
		return this.gmLevel;
	}
	
	public int getId(){
		return this.id;
	}
	
	public static List<CommandDto> getAllCommands(){
		List<Commands> commandsList = Arrays.asList(values());
		List<CommandDto> out = new ArrayList();
		for(Commands command : commandsList){
			out.add(new CommandDto(command.getId(),command.getCommand(),command.getGmLevel(),command.getSyntax()));
		}
		return out;
	}
	
	public static String getSyntaxForId(int id){
		List<Commands> commandsList = Arrays.asList(values());
		for(Commands command : commandsList){
			if(command.getId() == id){
				return command.getSyntax();
			}
		}
		return null;
	}
	
	public static CommandDto getAsDto(int id){
		List<Commands> commandsList = Arrays.asList(values());
		for(Commands command : commandsList){
			if(command.getId() == id){
				return new CommandDto(command.getId(),command.getCommand(),command.getGmLevel(),command.getSyntax());
			}
		}
		return null;
	}
	
	public static CommandDto getAsDto(String command){
		List<Commands> commandsList = Arrays.asList(values());
		for(Commands command_ : commandsList){
			if(command_.getCommand().equals(command)){
				return new CommandDto(command_.getId(),command_.getCommand(),command_.getGmLevel(),command_.getSyntax());
			}
		}
		return null;
	}
	
}
