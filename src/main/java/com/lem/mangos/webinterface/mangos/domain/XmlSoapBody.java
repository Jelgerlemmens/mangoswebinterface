package com.lem.mangos.webinterface.mangos.domain;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class XmlSoapBody {
	
	@JacksonXmlProperty(namespace = "ns1",localName = "executeCommandResponse")
	private XmlNs executeCommandResponse;
	
	
	public XmlNs getExecuteCommandResponse() {
		return executeCommandResponse;
	}
	
	public void setExecuteCommandResponse(XmlNs executeCommandResponse) {
		this.executeCommandResponse = executeCommandResponse;
	}
}
