package com.lem.mangos.webinterface.mangos.domain;

public class XmlResult {
	
	private String result;
	
	public String getResult() {
		return result;
	}
	
	public void setResult(String result) {
		this.result = result;
	}
}
