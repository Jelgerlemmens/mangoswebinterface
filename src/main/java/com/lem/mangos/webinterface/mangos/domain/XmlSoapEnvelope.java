package com.lem.mangos.webinterface.mangos.domain;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(namespace = "SOAP-ENV", localName = "Envelope")
public class XmlSoapEnvelope {
	
	@JacksonXmlProperty(localName="Body")
	private XmlSoapBody body;
	
	public XmlSoapBody getBody() {
		return body;
	}
	
	public void setBody(XmlSoapBody body) {
		this.body = body;
	}
}
