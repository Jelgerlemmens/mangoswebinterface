package com.lem.mangos.webinterface.mangos.service;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.lem.mangos.webinterface.mangos.domain.XmlSoapEnvelope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SoapService {
	
	SOAPConnectionFactory soapConnectionFactory;
	URL endpoint;
	String baseUrl;
	SOAPConnection connection;
	private static String user = "administrator";
	private static String pass = "ADMINISTRATOR";
	private static String soapMessage = "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsi=\"http://www.w3.org/1999/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/1999/XMLSchema\" xmlns:ns1=\"urn:MaNGOS\"><SOAP-ENV:Body><ns1:executeCommand><command>{comando}</command></ns1:executeCommand></SOAP-ENV:Body></SOAP-ENV:Envelope>";
	private static final String AUTH_HEADER_KEY = "Authorization";
	private static final String BASIC_AUTH_HEADER_VAL = "Basic";
	private ObjectMapper mapper;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SoapService.class);
	
	{
		mapper = XmlMapper.builder().build();
	}
	
	public void disconnect() throws SOAPException {
		connection.close();
	}
	
	public void createConnection(String baseUrl) throws SOAPException, MalformedURLException {
		this.baseUrl = baseUrl;
		soapConnectionFactory = SOAPConnectionFactory.newInstance();
		endpoint = new URL(baseUrl);
		connection = soapConnectionFactory.createConnection();
		MessageFactory.newInstance();
	}
	
	public XmlSoapEnvelope sendMessage(String command) throws SOAPException, IOException {
		soapMessage = soapMessage.replace("{comando}", command);
		SOAPMessage message = getSoapMessage();
		SOAPMessage responseSOAP = connection.call(message, endpoint);
		return parseSoapResponse(responseSOAP);
	}
	
	private SOAPMessage getSoapMessage() throws SOAPException, IOException {
		InputStream is = new ByteArrayInputStream(soapMessage.getBytes());
		SOAPMessage message = MessageFactory.newInstance().createMessage(null, is);
		MimeHeaders headers = message.getMimeHeaders();
		headers.addHeader(AUTH_HEADER_KEY, BASIC_AUTH_HEADER_VAL+" " + getBasicAuth());
		message.saveChanges();
		return message;
	}
	
	private String getBasicAuth(){
		String userAndPassword = String.format("%s:%s", user, pass);
		return Base64.getEncoder().encodeToString(userAndPassword.getBytes());
	}
	
	private XmlSoapEnvelope parseSoapResponse(SOAPMessage response) throws IOException, SOAPException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		response.writeTo(out);
		String str = new String(out.toByteArray());
		LOGGER.info("Response:: {}",str);
		return parseMessageContent(str);
	}
	
	
	public XmlSoapEnvelope parseMessageContent(String responseContent) throws JsonProcessingException {
		XmlSoapEnvelope result = mapper.readValue(responseContent,XmlSoapEnvelope.class);
		return result;
	}
	
	
	/*
			//SOAPConnectionFactoryImpl soapConnectionFactory = new SOAPConnectionFactoryImpl();
		SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
		URL endpoint = new URL(url);
		javax.xml.soap.SOAPConnection connection = soapConnectionFactory.createConnection();
		MessageFactory.newInstance();
		
		soapMessage = soapMessage.replace("{comando}", comando);
		String userAndPassword = String.format("%s:%s", user, pass);
		
		InputStream is = new ByteArrayInputStream(soapMessage.getBytes());
		SOAPMessage message = MessageFactory.newInstance().createMessage(null, is);
		
		String basicAuth = Base64.getEncoder().encodeToString(userAndPassword.getBytes());
		MimeHeaders headers = message.getMimeHeaders();
		
		headers.addHeader("Authorization", "Basic " + basicAuth);
		message.saveChanges();
		
		SOAPMessage responseSOAP = connection.call(message, endpoint);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		responseSOAP.writeTo(out);
		
		String response = new String(out.toByteArray());
		
		System.out.println("Result ::: " + response);
	 */
	
	
}
