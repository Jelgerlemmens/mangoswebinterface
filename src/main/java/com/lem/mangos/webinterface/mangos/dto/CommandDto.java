package com.lem.mangos.webinterface.mangos.dto;

public class CommandDto {
	private int id;
	private String command;
	private String gmLevel;
	private String syntax;
	
	public CommandDto(int id, String command, String gmLevel, String syntax) {
		this.id = id;
		this.command = command;
		this.gmLevel = gmLevel;
		this.syntax = syntax;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getCommand() {
		return command;
	}
	
	public void setCommand(String command) {
		this.command = command;
	}
	
	public String getGmLevel() {
		return gmLevel;
	}
	
	public void setGmLevel(String gmLevel) {
		this.gmLevel = gmLevel;
	}
	
	public String getSyntax() {
		return syntax;
	}
	
	public void setSyntax(String syntax) {
		this.syntax = syntax;
	}
}
