package com.lem.mangos.webinterface.mangos.service;

import java.util.List;

import com.lem.mangos.webinterface.mangos.domain.Commands;
import com.lem.mangos.webinterface.mangos.dto.CommandDto;


public class CommandService {
	
	public List<CommandDto> getCommands(){
		return Commands.getAllCommands();
	}
	
}
