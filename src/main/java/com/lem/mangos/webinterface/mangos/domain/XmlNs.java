package com.lem.mangos.webinterface.mangos.domain;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class XmlNs {
	
	
	@JacksonXmlProperty(localName="result")
	private String result;
	
	public String getResult() {
		return result;
	}
	
	public void setResult(String result) {
		this.result = result;
	}
}
