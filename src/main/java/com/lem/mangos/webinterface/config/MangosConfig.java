package com.lem.mangos.webinterface.config;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MangosConfig {
	
	@Value("${mangos.host}")
	private String MANGOS_HOST;
	
	@Value("${mangos.port}")
	private String MANGOS_PORT;
	
	
	
	@Bean
	public URI mangosHost() throws URISyntaxException {
		return new URI("https://"+MANGOS_HOST+":"+MANGOS_PORT);
	}
	
}
