package com.lem.mangos.webinterface.config;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.PostConstruct;

import com.lem.mangos.webinterface.web.domain.Authority;
import com.lem.mangos.webinterface.web.domain.AuthorityImpl;
import com.lem.mangos.webinterface.web.domain.UserDetailsImpl;
import com.lem.mangos.webinterface.web.service.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class UserConfig {
	
	@Autowired
	UserDetailsService userDetailsService;
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@PostConstruct
	public void createUser(){
		Collection<AuthorityImpl> auths = new ArrayList<>();
		auths.add(new AuthorityImpl(Authority.ADMIN.getAuthority()));
		UserDetailsImpl user = new UserDetailsImpl();
		user.setUsername("admin");
		user.setPassword(bCryptPasswordEncoder.encode("admin"));
		user.setAccountNonExpired(true);
		user.setCredentialsNonExpired(true);
		user.setAccountNonLocked(true);
		user.setEnabled(true);
		user.setAuthorities(auths);
		userDetailsService.save(user);
	}
}
