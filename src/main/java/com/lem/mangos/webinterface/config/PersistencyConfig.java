package com.lem.mangos.webinterface.config;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.sql.DataSource;

import com.lem.mangos.webinterface.cipher.CipherUtilAes;
import com.lem.mangos.webinterface.web.domain.Authority;
import com.lem.mangos.webinterface.web.domain.UserDetailsImpl;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.security.core.userdetails.UserDetails;

@Configuration
public class PersistencyConfig {
	
	@Value("${db.driver}")
	private String DB_DATASOURCE_DRIVER;
	
	@Value("${db.url}")
	private String DB_DATASOURCE_URL;
	
	@Value("${db.user}")
	private String DB_DATASOURCE_USER;
	
	@Value("${db.password}")
	private String DB_DATASOURCE_PASSWORD;
	
	@Value("${db.dialect}")
	private String DB_PROPERTIES_DIALECT;
	
	private static final String DB_PROPERTIES_DIALECT_KEY = "hibernate.dialect";
	
	@Value("#{new Boolean('${db.show_sql}')}")
	private boolean DB_PROPERTIES_SHOWSQL;
	
	private static final String DB_PROPERTIES_SHOWSQL_KEY = "hibernate.show_sql";
	
	@Value("${db.hbm2ddl}")
	private String DB_PROPERTIES_HBM2;
	
	@Autowired
	CipherUtilAes cipherUtilAes;
	
	private static final String DB_PROPERTIES_HBM2_KEY = "hibernate.hbm2ddl.auto";
	

	private Properties gethibernateProps() {
		Properties hibernateProperties = new Properties();
		hibernateProperties.put(DB_PROPERTIES_DIALECT_KEY, DB_PROPERTIES_DIALECT);
		hibernateProperties.put(DB_PROPERTIES_SHOWSQL_KEY, DB_PROPERTIES_SHOWSQL);
		hibernateProperties.put(DB_PROPERTIES_HBM2_KEY, DB_PROPERTIES_HBM2); //update //validate
		return hibernateProperties;
	}
	
	
	
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws BadPaddingException, InvalidKeyException, IllegalBlockSizeException, NoSuchPaddingException, NoSuchAlgorithmException {
		
		LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
		entityManagerFactoryBean.setJpaVendorAdapter(vendorAdaptor());
		entityManagerFactoryBean.setDataSource(dataSource());
		entityManagerFactoryBean.setPersistenceProviderClass(HibernatePersistenceProvider.class);
		entityManagerFactoryBean.setPackagesToScan("com.lem.mangos.webinterface.web.domain");
		entityManagerFactoryBean.setJpaProperties(gethibernateProps());
		
		return entityManagerFactoryBean;
	}
	
	
	@Bean
	public JpaTransactionManager transactionManager() throws BadPaddingException, InvalidKeyException, IllegalBlockSizeException, NoSuchAlgorithmException, NoSuchPaddingException {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
		return transactionManager;
	}
	
	private HibernateJpaVendorAdapter vendorAdaptor() {
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		return vendorAdapter;
	}
	
	
	@Bean
	public DataSource dataSource() throws BadPaddingException, InvalidKeyException, IllegalBlockSizeException, NoSuchAlgorithmException, NoSuchPaddingException {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(DB_DATASOURCE_DRIVER);
		dataSource.setUrl(DB_DATASOURCE_URL);
		dataSource.setUsername(DB_DATASOURCE_USER);
		dataSource.setPassword(cipherUtilAes.decode(DB_DATASOURCE_PASSWORD));
		return dataSource;
	}
}
