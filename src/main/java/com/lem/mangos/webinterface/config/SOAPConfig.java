package com.lem.mangos.webinterface.config;

import com.lem.mangos.webinterface.mangos.service.SoapService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SOAPConfig {
	
	@Bean
	public SoapService soapService(){return new SoapService();}
}
