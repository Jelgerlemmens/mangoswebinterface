package com.lem.mangos.webinterface.config;

import com.lem.mangos.webinterface.cipher.CipherUtilAes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class CipherConfig {
	
	@Bean
	BCryptPasswordEncoder bCryptPasswordEncoder(){return new BCryptPasswordEncoder();}
	
	
	@Bean
	CipherUtilAes cipherUtilAes(){return new CipherUtilAes();}
	
}
