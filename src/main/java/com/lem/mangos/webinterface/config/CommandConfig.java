package com.lem.mangos.webinterface.config;

import com.lem.mangos.webinterface.mangos.service.CommandService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CommandConfig {
	
	
	@Bean
	public CommandService commandService(){return new CommandService();}
	
}
