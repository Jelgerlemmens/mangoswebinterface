package com.lem.mangos.webinterface.cipher;

import static javax.crypto.Cipher.getInstance;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class CipherUtilAes {
	
	private static final String AES = "AES";
	private static final String AUTHORIZATION_KEY = "as3Df4VGs91J4fur";
	
	public String encode(String password)throws BadPaddingException, IllegalBlockSizeException,	InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {
		Cipher cipher = getInstance(AES);
		cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(AUTHORIZATION_KEY.getBytes(), AES));
		byte[] encVal = cipher.doFinal(password.getBytes());
		return new String(Base64.getEncoder().encode(encVal));
	}
	
	public String decode(String encryptedPassword) throws BadPaddingException, IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {
		Cipher cipher = getInstance(AES);
		cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(AUTHORIZATION_KEY.getBytes(), AES));
		byte[] decordedValue = Base64.getDecoder().decode(encryptedPassword);
		byte[] decValue = cipher.doFinal(decordedValue);
		return new String(decValue);
	}
	
}
